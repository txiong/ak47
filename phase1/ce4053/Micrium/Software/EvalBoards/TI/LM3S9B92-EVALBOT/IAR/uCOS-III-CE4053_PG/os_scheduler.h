#ifndef  OS_SCHEDULER_H
#define  OS_SCHEDULER_H
#include <os.h>

typedef struct _AVL_node{
  struct _AVL_node *left;
  struct _AVL_node *right;
  struct _AVL_node *nextptr;
  OS_TCB *tcb_ptr;
  CPU_INT08U   height;
} AVL_node;

void AVL_init();

void AVL_insertDeadline(OS_TCB *tcb_ptr);

void AVL_removeDeadline(OS_TCB *tcb_ptr);

AVL_node *AVL_getLowestDeadline();

CPU_INT08U     AVL_max(CPU_INT08U a, CPU_INT08U b);

CPU_INT08U     AVL_height(AVL_node *node);

AVL_node   *AVL_newNode(OS_TCB *tcb_ptr);

AVL_node   *AVL_rightRotate(AVL_node *node);

AVL_node   *AVL_leftRotate(AVL_node *node);

CPU_INT16S     AVL_getBalance(AVL_node *node);

AVL_node   *AVL_insertNode(AVL_node *node, OS_TCB *tcb_ptr);

AVL_node   *AVL_minValueNode(AVL_node *node, OS_TCB *tcb_ptr);

AVL_node   *AVL_deleteNode(AVL_node *root, OS_TCB *tcb_ptr);

AVL_node   *AVL_search(AVL_node *root, OS_TCB *tcb_ptr);

AVL_node *minDeadline(AVL_node *node);



#endif