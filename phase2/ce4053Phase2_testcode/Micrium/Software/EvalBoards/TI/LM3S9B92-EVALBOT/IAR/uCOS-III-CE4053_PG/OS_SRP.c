#include "OS_SRP.h"

//stack global variable
STACK s;
//red black global variable
//Red Black Tree Node
OS_MEM            redblacktreeNodeMEM;
RedBlackNode      alloMemredblackNode[40];   //1 row for 40 words of RedBlackNode

//Red Black Tree
OS_MEM            redblackTreeMEM;
RedBlackTree      alloMemredblackTree[1];    //1 row for 1 word of RedBlackTree

//Red Black recursive List reference
RedBlackTree *redBlackTree = (RedBlackTree *)0;

BL bList;

//----------------------------stack functions -----------------------------
void initStacks(){
  s.top = -1;
  bList.top = -1;
}

void push(OS_MUTEX i ){
    if (s.top == (STK_MAX - 1))
    {
        printf ("Stack is Full\n");
        return;
    }
    else
    {
        s.top = s.top + 1;
        s.stk[s.top] = i;
        int SC = getSystemCeiling();
        if(i.ResourceCeiling < SC){
          updateSystemCeiling(i.ResourceCeiling);
        }
        //printf ("pushed element is = %d \n", s.stk[s.top].ResourceCeiling);

    }
    return;
}

OS_MUTEX pop(){
   OS_MUTEX num;
   if (s.top == - 1)
    {
        printf ("Stack is Empty\n");
        return num;
    }
   else
   {
     num = s.stk[s.top];
     //printf ("poped element is = %d \n", s.stk[s.top].ResourceCeiling);
     s.top = s.top - 1;
   }
   return num;
}
//-------------------------------------------------------------------------
//-------------------------red black functions ----------------------------
void RedBlackTree_init(void)
{
    //Allocate Memory for red black Nodes
    
    OS_ERR err;
     
    printf("Initializing Red Black Tree\n");
    //Allocate Memory to binary heap nodes
    OSMemCreate((OS_MEM         *)&redblacktreeNodeMEM,
                (CPU_CHAR       *)"RedBlackTreeNodePartition",
                (void           *)&alloMemredblackNode[0],
                (OS_MEM_QTY      ) 40,
                (OS_MEM_SIZE     ) sizeof(RedBlackNode),
                (OS_ERR         *)&err);
    
    printf("Red Black Tree node memory allocated success\n");
    
    //Allocate Memory to red black tree
    OSMemCreate((OS_MEM         *)&redblackTreeMEM,
                (CPU_CHAR       *)"RedBlackTreePartition",
                (void           *)&alloMemredblackTree[0],
                (OS_MEM_QTY      ) 2,
                (OS_MEM_SIZE     ) sizeof(RedBlackTree),
                (OS_ERR         *)&err);
    
    //Allocate Memory to OS_USER_TCB
    // OSMemCreate((OS_MEM         *)&OS_USER_TCB_MEM,
                // (CPU_CHAR       *)"OS_USER_TCB",
                // (void           *)&alloMemOS_USER_TCB[0],
                // (OS_MEM_QTY      ) 40,
                // (OS_MEM_SIZE     ) sizeof(OS_USER_TCB),
                // (OS_ERR         *)&err);    
    
    
    //Initialize nodelists
    redBlackTree = (RedBlackTree*)OSMemGet(
                                      (OS_MEM  *)&redblackTreeMEM, 
                                      (OS_ERR  *)&err);
    
    redBlackTree->rootNode = (RedBlackNode*)0;
    redBlackTree->neel =  (RedBlackNode *)OSMemGet(&redblacktreeNodeMEM,
                                         &err);
    redBlackTree->neel->color = 1;
    
    printf("Red Black Tree memory allocated success\n");
}

//BST insertion
void RedBlackTree_insert(OS_TCB * _ptr_tcb)
{
  OS_ERR err;
  
  RedBlackNode *y = redBlackTree->neel;
  RedBlackNode *x = redBlackTree->rootNode;
  RedBlackNode *newNode = (RedBlackNode *)OSMemGet(&redblacktreeNodeMEM,
                                         &err);

  if(err == OS_ERR_NONE)
  {
    //printf("Node %d with time %d Created\n",_ptr_tcb->period,_ptr_tcb->deadline);
  }else
  {
    //printf("Node %d with time %d not Created\n",_ptr_tcb->period,_ptr_tcb->deadline); 
  }
  
  newNode->ptr_tcb = _ptr_tcb;
  
  if(redBlackTree->rootNode != (RedBlackNode*)0)
  {
    while(x != redBlackTree->neel)
    {
      y = x;
      if(newNode->ptr_tcb->deadline <= x->ptr_tcb->deadline)
      {
        x = x->left;
      }
      else
      {
        x = x->right;      
      }
    }
  }
  newNode->parent = y;
  
  if(y == redBlackTree->neel)
  {
    redBlackTree->rootNode = newNode;    
  }
  else if(newNode->ptr_tcb->deadline <= y->ptr_tcb->deadline)
  {
    y->left = newNode;    
  }
  else
  {
    y->right = newNode;
  }
  redBlackTree->curNumberOfElements++;
  newNode->left = redBlackTree->neel;
  newNode->right = redBlackTree->neel;
  newNode->color = (CPU_INT08U)0;
    
  RedBlackTree_InsertFix(newNode);
  
    //  printf("In insert, period = %d,time = %d\n",newNode->ptr_tcb->period,
    //         newNode->ptr_tcb->timeRemaining);
  
} 

//Red Black Tree deletion
void RedBlackTree_delete(OS_TCB *_ptr_tcb)
{
 
  OS_ERR err;
  
  //Check if the node is exist
  RedBlackNode *delete_node = search(_ptr_tcb);
  
  if(delete_node == (RedBlackNode*)0)
  {
    return;
  }
  
  //To be deleted and return memory 
  RedBlackNode *y = (RedBlackNode*)0;
  //The child to be deleted
  RedBlackNode *x = (RedBlackNode*)0;

  if(delete_node->left == redBlackTree->neel || delete_node->right == redBlackTree->neel)
  {
    y = delete_node;    
  }
  else
  {
    y = Successor(delete_node);
  }

  if(y->left != redBlackTree->neel)
  {
    x = y->left;
  }
  else
  {
    x = y->right;   
  }
  
  x->parent = y->parent;
  
  if(y->parent == redBlackTree->neel)
  {
    redBlackTree->rootNode = x;    
  }
  else if(y == y->parent->left)
  {
    y->parent->left = x;    
  }
  else
  {
   y->parent->right = x;     
  }
  
  if(y != delete_node)
  {
    delete_node->ptr_tcb = y->ptr_tcb;
  }
  
  if(y->color ==1)
  {
    RedBlackTree_DeleteFix(x);
  }
  
  //Returning memory 
   
  redBlackTree->curNumberOfElements--;

  
  OSMemPut((OS_MEM *)&redblacktreeNodeMEM,
           (void   *) y,
           (OS_ERR *) &err);
  
}

//print RedBlackTree in the pre-order manner
void RedBlackTree_printTree(void)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;  
  
  //Init Pointer
  
  RedBlackNode *temp = root;
  
  BSTRecursiveFinding(temp);
}

//Helper function

RedBlackNode* LeftMost(RedBlackNode* current)
{
  while(current->left != redBlackTree->neel)
  {
    current = current->left; 
  }
  return current;
}

RedBlackNode* Successor(RedBlackNode* current)
{
  if(current->right != redBlackTree->neel)
  {
    return LeftMost(current->parent);   
  }
  
  RedBlackNode *newNode = current->parent;
  
  while(newNode != redBlackTree->neel && current == newNode->right)
  {
    current = newNode;
    newNode = newNode->parent;    
  }
  return newNode;
}

//Fix the tree after insertion
void RedBlackTree_InsertFix(RedBlackNode *current)
{
  //case 0: if parent is black, no while loop
  
  while(current->parent->color == 0) //if parent is red, go into loop
  {
    //if parent is grandparent's left child
    
    if(current->parent == current->parent->parent->left)
    {
      RedBlackNode *uncle = current->parent->parent->right;
      
      //case 1
      if(uncle->color == 0)
      {
        current->parent->color = 1;
        uncle->color = 1;
        current->parent->parent->color = 0;
        current = current->parent->parent;        
      }
      //case 2 & 3 : uncle is black
      else
      {
        //case 2
        if(current == current->parent->right)
        {
          current = current->parent;
          RedBlackTree_LeftRotate(current);
        }
        //case 3
        current->parent->color = 1;                      //parent -> black
        current->parent->parent->color = 0;              //grandparent -> red
        RedBlackTree_RightRotate(current->parent->parent);
      }
    }
    //if parent is grandparent's right child
    else
    {
      RedBlackNode *uncle = current->parent->parent->left;
      
      //case 1
      if(uncle->color == 0)
      {
        current->parent->color = 1;
        uncle->color = 1;
        current->parent->parent->color = 0;
        current = current->parent->parent;        
      }
      //case 2 & 3 : uncle is black
      else
      {
        //case 2
        if(current == current->parent->left)
        {
          current = current->parent;
          RedBlackTree_RightRotate(current);
        }
        //case 3
        current->parent->color = 1;                      //parent -> black
        current->parent->parent->color = 0;              //grandparent -> red
        RedBlackTree_LeftRotate(current->parent->parent);
      }      
    }
  }
  redBlackTree->rootNode->color = 1;
}

//Fixed the Red Black Tree after deletion
void RedBlackTree_DeleteFix(RedBlackNode *current)
{
  //case 0: if current is red, change it to black 
  //        if current is root, change it to black
  
  while(current != redBlackTree->rootNode && current->color == 1)
  {
    //if current is leftchild
    if(current == current->parent->left)
    {
      RedBlackNode *sibling = current->parent->right;
      
      //case 1: if sibling is red
      if(sibling->color == 0)
      {
        sibling->color = 1;
        current->parent->color = 0;
        RedBlackTree_LeftRotate(current->parent);
        sibling = current->parent->right;        
      }
      
      //Enter case 2, 3 and 4 where sibling is black
      //case 2: if the two childs of sibling are black
      if(sibling->left->color == 1 && sibling->right->color ==1)
      {
        sibling->color = 0;
        //if current is updated to be root, exit the while loop
        current = current->parent;        
      }
      
      //case 3 and 4: there is only one child of current is black
      else
      {
        //case 3: sibling's right child is black and left child is red
        if(sibling->right->color == 1)
        {
            sibling->left->color = 1;
            sibling->color = 0;
            RedBlackTree_RightRotate(sibling);
            sibling = current->parent->right;
        }
        
        //case 4: sibling's left child is black and right child is black
        sibling->color = current->parent->color;
        current->parent->color = 1;
        sibling->right->color = 1;
        RedBlackTree_LeftRotate(current->parent);
        current = redBlackTree->rootNode;       //put current as rootnode and jump out of while loop
      }
    }
    //current is right child
    else
    {
      RedBlackNode *sibling = current->parent->left;
      //case 1: if sibling is red
      if(sibling->color == 0)
      {
        sibling->color = 1;
        current->parent->color = 0;
        RedBlackTree_RightRotate(current->parent);
        sibling = current->parent->left;        
      }
      //Enter case 2, 3 and 4 where sibling is black
      if(sibling->left->color == 1 && sibling->right->color == 1)
      {
        sibling->color = 0;
        current = current->parent;
      }
      else
      {
        //case 3: sibling's left child is black and right child is red
        if(sibling->left->color == 1)
        {
          sibling->right->color = 1;
          sibling->color = 0;
          RedBlackTree_LeftRotate(sibling);
          sibling = current->parent->left;  
        }
        //case 4: sibling's left child is red and right child is black
        sibling->color = current->parent->color;
        current->parent->color = 1;
        sibling->left->color = 1;
        RedBlackTree_RightRotate(current->parent);
        current = redBlackTree->rootNode;       //put current as rootnode and jump out of while loop
      }  
    }    
  }
  current->color = 1; 
}
              
//left rotate about the node
void RedBlackTree_LeftRotate(RedBlackNode *x)
{
   RedBlackNode *y = x->right;
   
   x->right = y->left;
   
   if(y->left != redBlackTree->neel)
   {
     y->left->parent = x;
   }
   
   y->parent = x->parent;
   
   if(x->parent == redBlackTree->neel)
   {
     redBlackTree->rootNode = y;
   }
   else if(x == x->parent->left)
   {
     x->parent->left = y;
   }
   else
   {
     x->parent->right = y;     
   }
   
   y -> left = x;
   x-> parent = y;
}
//right rotate about the node
void RedBlackTree_RightRotate(RedBlackNode *y)
{
  RedBlackNode *x = y->left;
  
  y->left = x->right;
  
  if(x->right != redBlackTree->neel)
  {
    x->right->parent = y;
  }
  
  x->parent = y->parent;
  
  if(y->parent == redBlackTree->neel)
  {
    redBlackTree->rootNode = x;
  }
  else if(y == y->parent->left)
  {
    y->parent->left = x;    
  }
  else
  {
    y->parent->right = x;    
  }
  
  x->right  = y;
  y->parent = x;
}

//Recursively printing nodes
void  BSTRecursiveFinding(RedBlackNode *curNode)
{
  if(curNode == redBlackTree->neel)
  {
      return;
  }
  
  //printf("Node: %d,%d\n",curNode->ptr_tcb->period,
  //       curNode->ptr_tcb->timeRemaining);
  
  BSTRecursiveFinding(curNode->left);
  
  BSTRecursiveFinding(curNode->right);
}


//Get the avaliable leaf node based on ptr tcb
RedBlackNode *BSTFindSlots(OS_TCB* _ptr_tcb)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;
  
  RedBlackNode *temp = (RedBlackNode*)0;
  
  //Root is null
  if(root == (RedBlackNode*)0)
  {
    return (RedBlackNode*)0;    
  }
  
  //start from root
  temp = root;
  
  //Break while loop if the current temp is the leaf node
  while( (temp->left  != redBlackTree->neel) &&
         (temp->right != redBlackTree->neel))
  {
    if(_ptr_tcb->deadline > temp->ptr_tcb->deadline)
    {
      temp = temp->right;      
    }
    else
    {
      temp = temp->left;
    }
  }  
  
  return temp;
}


RedBlackNode* search(OS_TCB *_ptr_tcb)
{
  RedBlackNode *temp = redBlackTree->rootNode;
  
  RedBlackNode *toBeReturned = (RedBlackNode*)0;
  
  while(temp != redBlackTree->neel)
  {
    
    if(temp->ptr_tcb->id == _ptr_tcb->id && 
       temp->ptr_tcb->deadline == _ptr_tcb->deadline)
    {
       //printf("In deletion, period = %d,time = %d\n",temp->ptr_tcb->period,
      //       temp->ptr_tcb->timeRemaining);
      toBeReturned = temp;
      break;      
    }   
    
    if(_ptr_tcb->deadline > temp->ptr_tcb->deadline)
    {
      temp = temp->right;      
    }
    else
    {
      temp = temp->left;
    }
   
  }  
    
  return toBeReturned;
}
//-------------------------------------------------------------------------

void  OSBlockedListCreate (OS_TCB *N)
{
  RedBlackTree_insert(N);
    //bList.top = bList.top + 1;
    //bList.stk[bList.top] = N;
  //insertRedBlackTree(Node N);
}

int  OSBlockedListRelease ()
{
  /*
  +++++++++++++++++++++++++++++++++++++++++++++++++++
  *For Benchmarking
  +++++++++++++++++++++++++++++++++++++++++++++++++++
  */
  BRStartTime = OS_TS_GET ();
  BRStartTime2 = OS_TS_GET ();
  /*+++++++++++++++++++++++++++++++++++++++++++++++++*/
  int sc = getSystemCeiling();
  int counter = 0 ;
  RedBlackNode *r;
  if(redBlackTree->curNumberOfElements != 0){
    r =  LeftMost(redBlackTree->rootNode);
  }
  else{
    return counter;
  }
  
  while(r->ptr_tcb->deadline < sc){
    OS_TCB *now = r->ptr_tcb;
    AVL_insertDeadline(now);
    counter++;
    RedBlackTree_delete(now);
    if(redBlackTree->curNumberOfElements != 0){
      r =  LeftMost(redBlackTree->rootNode);
    }
    else{
      break;
    }
    //OS_TCB *now = bList.stk[bList.top];
    //bList.top = bList.top - 1;
    //AVL_insertDeadline(now);
  }
  /*
  +++++++++++++++++++++++++++++++++++++++++++++++++++
  *For Benchmarking
  *OverheadValue = time difference - overhead for OS_TS_GET()
  +++++++++++++++++++++++++++++++++++++++++++++++++++
  */
  BROverheadValue = (OS_TS_GET() - BRStartTime2) - (BRStartTime2 - BRStartTime)  ;
  BROverheadValue = BROverheadValue / 50;
  if(Debug == 0){
    printf(": Overhead of Block Release time (%d) : %d us\n", counter, BROverheadValue);
  }
  /*+++++++++++++++++++++++++++++++++++++++++++++++++*/
  return counter;
}

//----------------------SRP MUTEX functions -------------------------------
void  SRP_OSMutexCreate (OS_MUTEX    *p_mutex,
                     CPU_CHAR    *p_name,
                     OS_ERR      *p_err,
                      int         rc)
{
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to be called from an ISR                   */
        *p_err = OS_ERR_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();
    p_mutex->Type              =  OS_OBJ_TYPE_MUTEX;        /* Mark the data structure as a mutex                     */
    p_mutex->NamePtr           =  p_name;
    p_mutex->OwnerTCBPtr       = (OS_TCB       *)0;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)0;         /* Mutex is available                                     */
    p_mutex->TS                = (CPU_TS        )0;
    p_mutex->OwnerOriginalPrio =  OS_CFG_PRIO_MAX;
    p_mutex->ResourceCeiling   = rc;
    OS_PendListInit(&p_mutex->PendList);                    /* Initialize the waiting list                            */

#if OS_CFG_DBG_EN > 0u
    OS_MutexDbgListAdd(p_mutex);
#endif
    OSMutexQty++;

    CPU_CRITICAL_EXIT();
    *p_err = OS_ERR_NONE;
}


void  SRP_OSMutexPend (OS_MUTEX   *p_mutex,
                   OS_TICK     timeout,
                   OS_OPT      opt,
                   CPU_TS     *p_ts,
                   OS_ERR     *p_err)
{
    /*
    +++++++++++++++++++++++++++++++++++++++++++++++++++
    *For Benchmarking
    +++++++++++++++++++++++++++++++++++++++++++++++++++
    */
    MAStartTime = OS_TS_GET ();
    MAStartTime2 = OS_TS_GET ();
    /*+++++++++++++++++++++++++++++++++++++++++++++++++*/
    
    OS_PEND_DATA  pend_data;
    OS_TCB       *p_tcb;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_PEND_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate arguments                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
    switch (opt) {
        case OS_OPT_PEND_BLOCKING:
        case OS_OPT_PEND_NON_BLOCKING:
             break;

        default:
             *p_err = OS_ERR_OPT_INVALID;
             return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    if (p_ts != (CPU_TS *)0) {
       *p_ts  = (CPU_TS  )0;                                /* Initialize the returned timestamp                      */
    }

    CPU_CRITICAL_ENTER();
    //------------------check with SC again-----------------
    if (p_mutex->OwnerNestingCtr == (OS_NESTING_CTR)0) {    /* Resource available?                                    */
      //if the current task's priority is bigger than SC
        //if any of the resource has the task's ptr
          //the allow
        //if dont have
          //dont allow
      //if not bigger than sc
        //then allow
      
      if(OSTCBCurPtr->deadline >= getSystemCeiling()){
        if(OSTCBCurPtr->mutexCounter > 0){
              p_mutex->OwnerTCBPtr       =  OSTCBCurPtr;          /* Yes, caller may proceed                                */
              p_mutex->OwnerOriginalPrio =  OSTCBCurPtr->Prio;
              p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
              if (p_ts != (CPU_TS *)0) {
                 *p_ts                   = p_mutex->TS;
              }
              //------------------push to stack-----------------
              if(Debug == 0){}
              else{
                if(OSTCBCurPtr->id  == 1){
                    printf("\t Resource %d allocated to task %d \n", p_mutex->NamePtr, OSTCBCurPtr->id);
                }
                else if(OSTCBCurPtr->id  == 2){
                    printf("\t\t Resource %d allocated to task %d \n",  p_mutex->NamePtr, OSTCBCurPtr->id);
                }
                else if(OSTCBCurPtr->id  == 3){
                    printf("\t\t\t Resource %d allocated to task %d \n",  p_mutex->NamePtr, OSTCBCurPtr->id);
                }
              }
              
              push(*p_mutex);
              CPU_CRITICAL_EXIT();
              *p_err                     =  OS_ERR_NONE;
              /*
              +++++++++++++++++++++++++++++++++++++++++++++++++++
              *For Benchmarking
              *OverheadValue = time difference - overhead for OS_TS_GET()
              +++++++++++++++++++++++++++++++++++++++++++++++++++
              */
              MAOverheadValue = (OS_TS_GET() - MAStartTime2) - (MAStartTime2 - MAStartTime)  ;
              MAOverheadValue = MAOverheadValue / 50;
              if(Debug == 0){
                printf(": Overhead of Mutex Acquire time (%d) : %d us\n", p_mutex->NamePtr, MAOverheadValue);
              }
              /*+++++++++++++++++++++++++++++++++++++++++++++++++*/
              return;
        }
        else{
          //this should not occurs at all if SRP is properly implemented
          //remove from the ready list and put into the block list then reschedule
          //printf("Resourced blocked to task %d \n", OSTCBCurPtr->id);
          OSTCBCurPtr->mutexCounter = OSTCBCurPtr->mutexCounter + 1;
          if(Debug == 0){
            
          }
          else{
            if(OSTCBCurPtr->id  == 1){
              printf("\t Resource %d blocked to task %d \n", p_mutex->NamePtr, OSTCBCurPtr->id);
            }
            else if(OSTCBCurPtr->id  == 2){
              printf("\t\t Resource %d blocked to task %d \n",  p_mutex->NamePtr, OSTCBCurPtr->id);
            }
            else if(OSTCBCurPtr->id  == 3){
              printf("\t\t\t Resource %d blocked to task %d \n",  p_mutex->NamePtr, OSTCBCurPtr->id);
            }
          }
          //OSTCBCurPtr->TaskState = OS_TASK_STATE_PEND;
          OS_RdyListRemove(OSTCBCurPtr);
          OSBlockedListCreate(OSTCBCurPtr);
          OS_CRITICAL_EXIT_NO_SCHED();

          OSSched();                                              /* Find the next highest priority task ready to run       */
          return;
        }
      }
      else{
        OSTCBCurPtr->mutexCounter = OSTCBCurPtr->mutexCounter + 1;
        p_mutex->OwnerTCBPtr       =  OSTCBCurPtr;          /* Yes, caller may proceed                                */
        p_mutex->OwnerOriginalPrio =  OSTCBCurPtr->Prio;
        p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
        if (p_ts != (CPU_TS *)0) {
           *p_ts                   = p_mutex->TS;
        }
        //------------------push to stack-----------------
        if(Debug == 0){}
        else{
              if(OSTCBCurPtr->id  == 1){
                  printf("\t Resource %d allocated to task %d \n", p_mutex->NamePtr, OSTCBCurPtr->id);
              }
              else if(OSTCBCurPtr->id  == 2){
                  printf("\t\t Resource %d allocated to task %d \n",  p_mutex->NamePtr, OSTCBCurPtr->id);
              }
              else if(OSTCBCurPtr->id  == 3){
                  printf("\t\t\t Resource %d allocated to task %d \n",  p_mutex->NamePtr, OSTCBCurPtr->id);
              }
        }
              
        push(*p_mutex);

        CPU_CRITICAL_EXIT();
        *p_err                     =  OS_ERR_NONE;
        /*
        +++++++++++++++++++++++++++++++++++++++++++++++++++
        *For Benchmarking
        *OverheadValue = time difference - overhead for OS_TS_GET()
        +++++++++++++++++++++++++++++++++++++++++++++++++++
        */
        MAOverheadValue = (OS_TS_GET() - MAStartTime2) - (MAStartTime2 - MAStartTime)  ;
        MAOverheadValue = MAOverheadValue / 50;
        if(Debug == 0){
          printf(": Overhead of Mutex Acquire time (%d) : %d us\n", p_mutex->NamePtr, MAOverheadValue);
        }
        /*+++++++++++++++++++++++++++++++++++++++++++++++++*/
        return;
      }
    }

    if (OSTCBCurPtr == p_mutex->OwnerTCBPtr) {              /* See if current task is already the owner of the mutex  */
        p_mutex->OwnerNestingCtr++;
        if (p_ts != (CPU_TS *)0) {
           *p_ts  = p_mutex->TS;
        }
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_OWNER;                        /* Indicate that current task already owns the mutex      */
        return;
    }

    if ((opt & OS_OPT_PEND_NON_BLOCKING) != (OS_OPT)0) {    /* Caller wants to block if not available?                */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_PEND_WOULD_BLOCK;                   /* No                                                     */
        return;
    } else {
        if (OSSchedLockNestingCtr > (OS_NESTING_CTR)0) {    /* Can't pend when the scheduler is locked                */
            CPU_CRITICAL_EXIT();
            *p_err = OS_ERR_SCHED_LOCKED;
            return;
        }
    }

    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();                  /* Lock the scheduler/re-enable interrupts                */
    p_tcb = p_mutex->OwnerTCBPtr;                           /* Point to the TCB of the Mutex owner                    */
    if (p_tcb->Prio > OSTCBCurPtr->Prio) {                  /* See if mutex owner has a lower priority than current   */
        switch (p_tcb->TaskState) {
            case OS_TASK_STATE_RDY:
                 OS_RdyListRemove(p_tcb);                   /* Remove from ready list at current priority             */
                 p_tcb->Prio = OSTCBCurPtr->Prio;           /* Raise owner's priority                                 */
                 //AVL_insertprio(p_tcb->Prio);
                 OS_PrioInsert(p_tcb->Prio);
                 OS_RdyListInsertHead(p_tcb);               /* Insert in ready list at new priority                   */
                 break;

            case OS_TASK_STATE_DLY:
            case OS_TASK_STATE_DLY_SUSPENDED:
            case OS_TASK_STATE_SUSPENDED:
                 p_tcb->Prio = OSTCBCurPtr->Prio;           /* Only need to raise the owner's priority                */
                 break;

            case OS_TASK_STATE_PEND:                        /* Change the position of the task in the wait list       */
            case OS_TASK_STATE_PEND_TIMEOUT:
            case OS_TASK_STATE_PEND_SUSPENDED:
            case OS_TASK_STATE_PEND_TIMEOUT_SUSPENDED:
                 OS_PendListChangePrio(p_tcb,
                                       OSTCBCurPtr->Prio);
                 break;

            default:
                 OS_CRITICAL_EXIT();
                 *p_err = OS_ERR_STATE_INVALID;
                 return;
        }
    }

    OS_Pend(&pend_data,                                     /* Block task pending on Mutex                            */
            (OS_PEND_OBJ *)((void *)p_mutex),
             OS_TASK_PEND_ON_MUTEX,
             timeout);

    OS_CRITICAL_EXIT_NO_SCHED();

    OSSched();                                              /* Find the next highest priority task ready to run       */

    CPU_CRITICAL_ENTER();
    switch (OSTCBCurPtr->PendStatus) {
        case OS_STATUS_PEND_OK:                             /* We got the mutex                                       */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_NONE;
             break;

        case OS_STATUS_PEND_ABORT:                          /* Indicate that we aborted                               */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_PEND_ABORT;
             break;

        case OS_STATUS_PEND_TIMEOUT:                        /* Indicate that we didn't get mutex within timeout       */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = (CPU_TS  )0;
             }
             *p_err = OS_ERR_TIMEOUT;
             break;

        case OS_STATUS_PEND_DEL:                            /* Indicate that object pended on has been deleted        */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_OBJ_DEL;
             break;

        default:
             *p_err = OS_ERR_STATUS_INVALID;
             break;
    }
    CPU_CRITICAL_EXIT();
}


void  SRP_OSMutexPost (OS_MUTEX  *p_mutex,
                   OS_OPT     opt,
                   OS_ERR    *p_err)
{
    
     /*
    +++++++++++++++++++++++++++++++++++++++++++++++++++
    *For Benchmarking
    +++++++++++++++++++++++++++++++++++++++++++++++++++
    */
    MRStartTime = OS_TS_GET ();
    MRStartTime2 = OS_TS_GET ();
    /*+++++++++++++++++++++++++++++++++++++++++++++++++*/
    
    OS_PEND_LIST  *p_pend_list;
    OS_TCB        *p_tcb;
    CPU_TS         ts;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_POST_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();

    if (OSTCBCurPtr != p_mutex->OwnerTCBPtr) {              /* Make sure the mutex owner is releasing the mutex       */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_NOT_OWNER;
        return;
    }
    
    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();
    ts          = OS_TS_GET();                              /* Get timestamp                                          */
    p_mutex->TS = ts;
    p_mutex->OwnerNestingCtr--;                             /* Decrement owner's nesting counter                      */
    //------------update system ceiling and pop -----------------
    if(Debug == 0){}
    else{
              if(OSTCBCurPtr->id  == 1){
                  printf("\t Resource %d deallocated by task %d \n", p_mutex->NamePtr, OSTCBCurPtr->id);
              }
              else if(OSTCBCurPtr->id  == 2){
                  printf("\t\t Resource %d deallocated by task %d \n",  p_mutex->NamePtr, OSTCBCurPtr->id);
              }
              else if(OSTCBCurPtr->id  == 3){
                  printf("\t\t\t Resource %d deallocated by task %d \n",  p_mutex->NamePtr, OSTCBCurPtr->id);
              }
    }
            
    int sc = getSystemCeiling();
    if(s.stk[s.top].ResourceCeiling > sc){
      OSTCBCurPtr->mutexCounter = OSTCBCurPtr->mutexCounter - 1;
      pop();
    }
    else{
      //this portion is for not well nested 
      //not properly integrated yet
      OSTCBCurPtr->mutexCounter = OSTCBCurPtr->mutexCounter - 1;
      pop();
      //update SC
      if(s.top == -1){
        updateSystemCeiling(255);
      }
      else{
        int min = s.stk[s.top].ResourceCeiling;
        for(int a = 0; a < s.top; a++){
          if(s.stk[a].ResourceCeiling<min){
            min = s.stk[a].ResourceCeiling;
          }
        }
        updateSystemCeiling(min);
      }
    }
    if ( OSBlockedListRelease() > 0){
      OS_CRITICAL_EXIT_NO_SCHED();
      OSSched();
    }
    //----------------------------------------------------------

    if (p_mutex->OwnerNestingCtr > (OS_NESTING_CTR)0) {     /* Are we done with all nestings?                         */
        OS_CRITICAL_EXIT();                                 /* No                                                     */
        *p_err = OS_ERR_MUTEX_NESTING;
        return;
    }

    p_pend_list = &p_mutex->PendList;
    if (p_pend_list->NbrEntries == (OS_OBJ_QTY)0) {         /* Any task waiting on mutex?                             */
        p_mutex->OwnerTCBPtr     = (OS_TCB       *)0;       /* No                                                     */
        p_mutex->OwnerNestingCtr = (OS_NESTING_CTR)0;
        OS_CRITICAL_EXIT();
        *p_err = OS_ERR_NONE;
        /*
        +++++++++++++++++++++++++++++++++++++++++++++++++++
        *For Benchmarking
        *OverheadValue = time difference - overhead for OS_TS_GET()
        +++++++++++++++++++++++++++++++++++++++++++++++++++
        */
        MROverheadValue = (OS_TS_GET() - MRStartTime2) - (MRStartTime2 - MRStartTime)  ;
        MROverheadValue = MROverheadValue / 50;
        if(Debug == 0){
          printf(": Overhead of Mutex Release time (%d) : %d us\n", p_mutex->NamePtr, MAOverheadValue);
        }
        /*+++++++++++++++++++++++++++++++++++++++++++++++++*/
        return;
    }
                                                            /* Yes                                                    */
    if (OSTCBCurPtr->Prio != p_mutex->OwnerOriginalPrio) {
        OS_RdyListRemove(OSTCBCurPtr);
        OSTCBCurPtr->Prio = p_mutex->OwnerOriginalPrio;     /* Lower owner's priority back to its original one        */
        //AVL_insertprio(OSTCBCurPtr->Prio);
        OS_PrioInsert(OSTCBCurPtr->Prio);
        OS_RdyListInsertTail(OSTCBCurPtr);                  /* Insert owner in ready list at new priority             */
        OSPrioCur         = OSTCBCurPtr->Prio;
    }
                                                            /* Get TCB from head of pend list                         */
    p_tcb                      = p_pend_list->HeadPtr->TCBPtr;
    p_mutex->OwnerTCBPtr       = p_tcb;                     /* Give mutex to new owner                                */
    p_mutex->OwnerOriginalPrio = p_tcb->Prio;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
                                                            /* Post to mutex                                          */
    OS_Post((OS_PEND_OBJ *)((void *)p_mutex),
            (OS_TCB      *)p_tcb,
            (void        *)0,
            (OS_MSG_SIZE  )0,
            (CPU_TS       )ts);

    OS_CRITICAL_EXIT_NO_SCHED();
    if ((opt & OS_OPT_POST_NO_SCHED) == (OS_OPT)0) {
        OSSched();                                          /* Run the scheduler                                      */
    }

    *p_err = OS_ERR_NONE;
}

//---------------------------------------------------------------------------