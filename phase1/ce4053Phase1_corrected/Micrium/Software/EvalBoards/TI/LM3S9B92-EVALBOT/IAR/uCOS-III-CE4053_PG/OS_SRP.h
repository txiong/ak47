#ifndef OS_SRP_H
#define OS_SRP_H

#include <stdio.h>

#define STK_MAX 5

//--------------stack typedef --------------- 
struct stack
{
    int stk[STK_MAX];
    int top;
};
typedef struct stack STACK;
//--------------------------------------------

//--------------stack function def------------ 
void push(int i);
int pop();
//--------------------------------------------

#endif