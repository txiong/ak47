#include "os_scheduler.h"

AVL_node        *AVL_root;
OS_MEM          AVL_Partition;
AVL_node        AVL_array[OS_CFG_PRIO_MAX];

// Initialize AVL tree
void AVL_init(){
  OS_ERR      err;
  OS_PRIO       i;
  AVL_node  *node;
  AVL_root = (AVL_node *)0;
  AVL_root->nextptr = (AVL_node *)0;
  AVL_root->left = (AVL_node *)0;
  AVL_root->right = (AVL_node *)0;

    for (i = 0u; i < OS_CFG_PRIO_MAX; i++) {                /* Initialize the array of OS_RDY_LIST at each priority   */
        node = &AVL_array[i];
        node->height = 0;
        node->left = (AVL_node *)0;
        node->right = (AVL_node *)0;
        node->tcb_ptr = (OS_TCB *)0;
        node->nextptr = (AVL_node *)0;
    }
    
    OSMemCreate((OS_MEM         *)&AVL_Partition,
                (CPU_CHAR       *)"AVL Partition",
                (void           *)&AVL_array[0],
                (OS_MEM_QTY      ) OS_CFG_PRIO_MAX,
                (OS_MEM_SIZE     ) sizeof(AVL_node),
                (OS_ERR         *)&err);
}

// Insert node into the AVL tree, the key is the absolute deadline
void AVL_insertDeadline(OS_TCB *tcb_ptr){
  
    // Search whether there is any existing same value deadline
    AVL_node  *node = AVL_search(AVL_root, tcb_ptr);
    
    // if cannot find any existing node
    if (node ==  (AVL_node *)0){
      // Insert the TCB into the AVL tree
      AVL_root = AVL_insertNode(AVL_root, tcb_ptr); 
    }
    else { 
      // there is duplicated node, chain the node to the existing node
      AVL_node  *temp = AVL_newNode(tcb_ptr);
      if (node->nextptr == (AVL_node *)0){
        node->nextptr = temp;
        temp->nextptr = (AVL_node *)0;
      }
      else {
        while (node->nextptr != (AVL_node *)0){
          node = node->nextptr;
        }
        node->nextptr = temp;
        temp->nextptr = (AVL_node *)0;
      }
    }
}

// Delete the node from the AVL tree
void AVL_removeDeadline(OS_TCB *tcb_ptr){
  AVL_root = AVL_deleteNode(AVL_root, tcb_ptr);
}

// Get the smallest deadline in the AVL tree
AVL_node *AVL_getLowestDeadline(){
   return minDeadline(AVL_root);
}

// Internal use in the AVL tree
CPU_INT08U AVL_max(CPU_INT08U a, CPU_INT08U b){
    return (a > b) ? a : b;
}

// Internal use in the AVL tree
CPU_INT08U AVL_height(AVL_node *node){
    if (node == (AVL_node*)0)
      return 0;
    return node->height;
}

// Creating a new node
AVL_node *AVL_newNode(OS_TCB *tcb_ptr){
    OS_ERR err;
    AVL_node *node = (AVL_node*)OSMemGet((OS_MEM  *)&AVL_Partition, (OS_ERR  *)&err);
    if (err == OS_ERR_NONE){
      node->left = (AVL_node*)0;
      node->right = (AVL_node*)0;
      node->tcb_ptr = tcb_ptr;
      node->height = 0;
      node->nextptr = (AVL_node *)0;
    }
    
    return node;
}

// Internal use in the AVL tree
AVL_node *AVL_rightRotate(AVL_node *node){
    AVL_node *x1 = node->left;
    AVL_node *temp = x1->right;
    
    // Perform rotation
    x1->right = node;
    node->left = temp;
    
    // update heights
    node->height = AVL_max(AVL_height(node->left), AVL_height(node->right)) + 1;
    x1->height = AVL_max(AVL_height(x1->left), AVL_height(x1->right)) + 1;
    
    return x1;
}

// Internal use in the AVL tree
AVL_node *AVL_leftRotate(AVL_node *node){
    AVL_node *y1 = node->right;
    AVL_node *temp = y1->left;
    
    // perform rotation
    y1->left = node;
    node->right = temp;
    
    // update heights
    node->height = AVL_max(AVL_height(node->left), AVL_height(node->right)) + 1;
    y1->height = AVL_max(AVL_height(y1->left), AVL_height(y1->right)) + 1;
    
    return y1;
}

// Internal use in the AVL tree
CPU_INT16S AVL_getBalance(AVL_node *node){
    if (node == (AVL_node *)0)
      return 0;
    return AVL_height(node->left) - AVL_height(node->right);
}

// Internal use in the AVL tree
AVL_node *AVL_insertNode(AVL_node *node, OS_TCB *tcb_ptr){
    // do the Balanced Search Tree rotation
    if (node == (AVL_node*)0)
      return (AVL_newNode(tcb_ptr));
    
    if (tcb_ptr->deadline < node->tcb_ptr->deadline)
      node->left = AVL_insertNode(node->left, tcb_ptr);
    else if (tcb_ptr->deadline > node->tcb_ptr->deadline)
      node->right = AVL_insertNode(node->right, tcb_ptr);
    else
      return node;
    
    // update height of the parent node
    node->height = 1 + AVL_max(AVL_height(node->left),
                               AVL_height(node->right));
    
    // get the balance factor of this ancestor node to check whether this node became unbalanced
    CPU_INT16S balance = AVL_getBalance(node);
    
    // check if this node becomes unbalanced, then there are 4 different cases
    
    // left Left Case
    if (balance > 1 && tcb_ptr->deadline < node->left->tcb_ptr->deadline)
      return AVL_rightRotate(node);
    
    // right Right Case
    if (balance < -1 && tcb_ptr->deadline > node->right->tcb_ptr->deadline)
      return AVL_leftRotate(node);
    
    // left Right Case
    if (balance > 1 && tcb_ptr->deadline > node->left->tcb_ptr->deadline){
      node->left = AVL_leftRotate(node->left);
      return AVL_rightRotate(node);
    }
    
    // right Left Case
    if (balance < -1 && tcb_ptr->deadline < node->right->tcb_ptr->deadline){
      node->right = AVL_rightRotate(node->right);
      return AVL_leftRotate(node);
    }
    
    // unchanged
    return node;
}

// Internal use in the AVL tree
AVL_node *AVL_minValueNode(AVL_node *node, OS_TCB *tcb_ptr){
    AVL_node *current = node;
    
    // tranverse to the left most node
    while (current->left != (AVL_node *)0){
      current = current->left;
    }
    
    return current;
}

// Remove the node from the AVL tree
AVL_node   *AVL_deleteNode(AVL_node *root, OS_TCB *tcb_ptr){
    // do standard BST delete
    if (root == (AVL_node *)0)
      return root;
    
    // if the key to be deleted is smaller than the
    // root's key, then it lies in left subtree
    if (tcb_ptr->deadline < root->tcb_ptr->deadline)
      root->left = AVL_deleteNode(root->left, tcb_ptr);
    
    // if the key to be deleted is greater than the
    // root's key, then it ies in right subtree
    else if (tcb_ptr->deadline > root->tcb_ptr->deadline)
      root->right = AVL_deleteNode(root->right, tcb_ptr);
    // if key is same as root's key, then This is
    // the node to be deleted
    else {
      // node with only one child or no child
      if ((root->left == (AVL_node *)0) || (root->right == (AVL_node *)0)){
        AVL_node * temp = root->left ? root->left : root->right;
        
        // no child case
        if (temp == (AVL_node *)0){
          temp = root;
          root = (AVL_node *)0;
        }
        else // one child case
          *root = *temp; // copy the contents of the non-empty child
        
        // free memory
        OS_ERR err;
        
        temp->height = 0;
        temp->left = (AVL_node *)0;
        temp->right = (AVL_node *)0;
        temp->tcb_ptr = (OS_TCB *)0;
        
        AVL_node * head = temp;
        AVL_node * current = temp->nextptr;
        while (current != (AVL_node *)0){
          temp->nextptr = current->nextptr;
          current->nextptr = (AVL_node *)0;
          OSMemPut((OS_MEM *)&AVL_Partition, (void *)current, (OS_ERR *)&err);
          current = temp->nextptr;
        }
       
        temp->nextptr = (AVL_node *)0;
        OSMemPut((OS_MEM *)&AVL_Partition, (void *)head, (OS_ERR *)&err);
      }
      else {
        // node with two children: Get the inorder
        // successor (smallest in the right subtree)
        AVL_node *temp = AVL_minValueNode(root->right, tcb_ptr);
        
        // copy the inorder successor's data to this node
        root->tcb_ptr = temp->tcb_ptr;
        
        // delete the inorder successor
        root->right = AVL_deleteNode(root->right, temp->tcb_ptr);
      }
    }
    
    // if the tree had only one node then return
    if (root == (AVL_node *)0)
      return root;
    
    // update height of the current node
    root->height = 1 + AVL_max(AVL_height(root->left), AVL_height(root->right));
    
    // get the balance factor of this node (to check whether this node became unbalanced
    CPU_INT16S balance = AVL_getBalance(root);
    
    // if this node becomes unbalanced, then there are 4 cases
    // left Left Case
    if (balance > 1 && AVL_getBalance(root->left) >= 0)
      return AVL_rightRotate(root);
    
    // left Right Case
    if (balance > 1 && AVL_getBalance(root->left) < 0){
      root->left = AVL_leftRotate(root->left);
      return AVL_rightRotate(root);
    }
    
    // right Right Case
    if (balance < -1 && AVL_getBalance(root->right) <= 0){
      return AVL_leftRotate(root);
    }
    
    // right Left Case
    if (balance < -1 && AVL_getBalance(root->right) > 0){
      root->right = AVL_rightRotate(root->right);
      return AVL_leftRotate(root);
    }
    
    // unchanged
    return root;
}

// Search the node based on deadline, return NULL if unable find the node 
AVL_node   *AVL_search(AVL_node *root, OS_TCB *tcb_ptr){
    if (root == (AVL_node *)0)
      return (AVL_node *)0;
    
    if (tcb_ptr->deadline == root->tcb_ptr->deadline)
      return root;
    else if (tcb_ptr->deadline < root->tcb_ptr->deadline)
      AVL_search(root->left, root->tcb_ptr);
    else if (tcb_ptr->deadline > root->tcb_ptr->deadline)
      AVL_search(root->right, root->tcb_ptr);
}

// Get the min deadline node
AVL_node *minDeadline(AVL_node *node){
  AVL_node* current = node;
  
  if (current == (AVL_node *)0){
    return (AVL_node *)0;
  }
  
  /* loop down to find the leftmost leaf */
  while (current->left != (AVL_node *)0) {
    current = current->left;
  }
  
  if (current == (AVL_node *)0){
    return (AVL_node *)0;
  }
  
  return (current);
}


