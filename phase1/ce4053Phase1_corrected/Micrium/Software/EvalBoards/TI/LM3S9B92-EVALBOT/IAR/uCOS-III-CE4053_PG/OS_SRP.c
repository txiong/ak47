#include "OS_SRP.h"

#include <stdio.h>

STACK s;

//----------------------------stack functions -----------------------------
void push(int i){
    if (s.top == (STK_MAX - 1))
    {
        printf ("Stack is Full\n");
        return;
    }
    else
    {
        s.top = s.top + 1;
        s.stk[s.top] = i;
        printf ("pushed element is = %d \n", s.stk[s.top]);

    }
    return;
}

int pop(){
   int num;
   if (s.top == - 1)
    {
        printf ("Stack is Empty\n");
        return (s.top);
    }
   else
   {
     num = s.stk[s.top];
     printf ("poped element is = %d \n", s.stk[s.top]);
     s.top = s.top - 1;
   }
   return num;
}