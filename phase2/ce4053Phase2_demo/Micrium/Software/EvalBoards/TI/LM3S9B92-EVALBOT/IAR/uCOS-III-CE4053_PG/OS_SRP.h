#ifndef OS_SRP_H
#define OS_SRP_H
#include "OS_RecTask.h"
#include <stdio.h>

#define STK_MAX 10

//--------------stack typedef --------------- 
struct stack
{
    OS_MUTEX stk[STK_MAX];
    int top;
};
typedef struct stack STACK;

struct bl
{
    OS_TCB* stk[STK_MAX];
    int top;
};
typedef struct bl BL;

//--------------------------------------------

//--------------redblack tree ----------------
typedef struct _redblackNode{
    OS_TCB* ptr_tcb;
    CPU_INT08U  color;  //0 for red, 1 for black
    struct _redblackNode *left;
    struct _redblackNode *right;
    struct _redblackNode *parent;   
}RedBlackNode;
typedef struct _redblacktree{
    RedBlackNode *rootNode;
    RedBlackNode *neel;
    CPU_INT08U curNumberOfElements;
    CPU_INT08U maxNumberOfElements;
}RedBlackTree;
//-------------------------------------------

//--------------stack function def------------ 
void push(OS_MUTEX i);
OS_MUTEX pop();
void  initinitStacks();
void  OSBlockedListCreate (OS_TCB *N);
int  OSBlockedListRelease();
//--------------------------------------------

/*
************************************************************************************************************************
*                                                 FUNCTION PROTOTYPE
************************************************************************************************************************
*/
  
//Allocate memory block and initialize
void RedBlackTree_init(void);

//Red Black Tree insertion
void RedBlackTree_insert(OS_TCB *);

//Red Black Tree deletion
void RedBlackTree_delete(OS_TCB *);

//print RedBlackTree in the pre-order manner
void RedBlackTree_printTree(void);


//Update timeRemaining
void RedBlackTree_updateTime(void);


/*
* Helper function
*/

RedBlackNode *BSTFindSlots(OS_TCB*);
void  BSTRecursiveFinding(RedBlackNode *);
void RedBlackTree_LeftRotate(RedBlackNode *);
void RedBlackTree_RightRotate(RedBlackNode *);
void RedBlackTree_InsertFix(RedBlackNode *);
void RedBlackTree_DeleteFix(RedBlackNode *);
RedBlackNode* search(OS_TCB *);
RedBlackNode* Successor(RedBlackNode*);
RedBlackNode* LeftMost(RedBlackNode*);


void  SRP_OSMutexCreate (OS_MUTEX    *p_mutex,
                     CPU_CHAR    *p_name,
                     OS_ERR      *p_err,
                      int         rc);
void  SRP_OSMutexPend (OS_MUTEX   *p_mutex,
                   OS_TICK     timeout,
                   OS_OPT      opt,
                   CPU_TS     *p_ts,
                   OS_ERR     *p_err);

void  SRP_OSMutexPost (OS_MUTEX  *p_mutex,
                   OS_OPT     opt,
                   OS_ERR    *p_err);


#endif