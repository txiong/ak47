#include "OS_RecTask.h"
#include <stdio.h>

NODE LIST[20];
int size = 0;
int max = 20;

void swap(NODE *node1, NODE *node2){
  NODE temp = *node1;
  *node1 = *node2;
  *node2 = temp;
}
//OSTaskCreate((OS_TCB     *)&AppTaskDispatchTCB, 
//                 (CPU_CHAR   *)"App Task Dispatch", 
//                 (OS_TASK_PTR ) AppTaskDispatch, 
//                 (void       *) 0, 
//                 (OS_PRIO     ) APP_TASK_DISPATCH_PRIO, 
//                 (CPU_STK    *)&AppTaskDispatchStk[0], 
//                 (CPU_STK_SIZE) APP_TASK_DISPATCH_STK_SIZE / 10u, 
//                 (CPU_STK_SIZE) APP_TASK_DISPATCH_STK_SIZE, 
//                 (OS_MSG_QTY  ) 0u, 
//                 (OS_TICK     ) 0u, 
//                 (void       *)(CPU_INT32U) 1, 
//                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
//                 (OS_ERR     *)&err);
    
void insert1( OS_TCB        *p_tcb,
              CPU_CHAR      *p_name,
              OS_TASK_PTR    p_task,
              void          *p_arg,
              OS_PRIO        prio,
              CPU_STK       *p_stk_base,
              CPU_STK_SIZE   stk_limit,
              CPU_STK_SIZE   stk_size,
              OS_MSG_QTY     q_size,
              OS_TICK        time_quanta,
              void          *p_ext,
              OS_OPT         opt,
              int id,
              int time, 
              int interval,
              int deadline){
  if(size < max){
    NODE temp;
    temp.p_tcb = p_tcb;
    temp.p_name = p_name;
    temp.p_task = p_task;
    temp.p_arg = p_arg;
    temp.prio = prio;
    temp.p_stk_base = p_stk_base;
    temp.stk_limit = stk_limit;
    temp.stk_size = stk_size;
    temp.time_quanta = time_quanta;
    temp.p_ext = p_ext;
    temp.opt = opt;
    temp.id = id;
    temp.time = time;
    temp.interval = interval;
    temp.deadline = deadline;
    
    int i = size++;
    while(i && temp.time < LIST[PARENT(i)].time){
      LIST[i]=LIST[PARENT(i)];
      i = PARENT(i); 
    }
    LIST[i] = temp;
  }
  
}

//Insert into the minimum binary heap
void insert(int id,int time, int interval,int deadline,OS_PRIO prio){
  if(size < max){
    NODE temp;
    temp.time = time;
    temp.prio = prio;
    temp.id = id;
    temp.interval = interval;
    
    int i = size++;
    while(i && temp.time < LIST[PARENT(i)].time){
      LIST[i]=LIST[PARENT(i)];
      i = PARENT(i); 
    }
    LIST[i] = temp;
  }
  
}

//Update time and heapify binary heap
void update(){
  LIST[0].time = LIST[0].time+LIST[0].interval;
  heapify( 0 );
}

//Heapify the binary heap
void heapify(int root) {
	int smallest = (LCHILD(root) < size && LIST[LCHILD(root)].time < LIST[root].time) ? LCHILD(root) : root;
	if (RCHILD(root) < size && LIST[RCHILD(root)].time < LIST[smallest].time) {
		smallest = RCHILD(root);
	}
	if (smallest != root) {
		swap(&(LIST[root]), &(LIST[smallest]));
		heapify(smallest);
	}
}

//Get the root node
NODE *getMin(){
  return &LIST[0];
}

//start recursive task
void recursiveTask(){
    int         t = 0;
    OS_ERR      err;
    CPU_TS      ts;
    OS_SEM_CTR  ctr;

    NODE *n;
    int id=99;
    while(1){
      
      StartTime = OS_TS_GET ();
      StartTime2 = OS_TS_GET ();
      n = getMin();                     // Get the min period node

      while(n->time == t){
          id = n->id;
           OSRecTaskCreate(n->p_tcb,
                            n->p_name,
                            n->p_task,
                            n->p_arg,
                            n->prio,
                            n->p_stk_base,
                            n->stk_limit,
                            n->stk_size,
                            n->q_size,
                            n->time_quanta,
                            n->p_ext,
                            n->opt, 
                            (OS_ERR     *)&err,
                            n->time + n->deadline,
                            n->id);
          update(); // heapify the binary heap
      }
      
      OverheadValue = StartTime - StartTime2 - StartTime2 + OS_TS_GET () ;
      OverheadValue = OverheadValue / 50;
      
      /*wait for sem every 1 second*/
      ctr= OSSemPend(   &CounterSemaphore,
                        0,
                        OS_OPT_PEND_BLOCKING,
                        &ts,
                        &err);
      t++;
      //printf("\n");
  }
}


// Initialise the TCB and insert the task into the AVL tree
void  OSRecTaskCreate (	OS_TCB        *p_tcb,
                        CPU_CHAR      *p_name,
                        OS_TASK_PTR    p_task,
                        void          *p_arg,
                        OS_PRIO        prio,
                        CPU_STK       *p_stk_base,
                        CPU_STK_SIZE   stk_limit,
                        CPU_STK_SIZE   stk_size,
                        OS_MSG_QTY     q_size,
                        OS_TICK        time_quanta,
                        void          *p_ext,
                        OS_OPT         opt,
                        OS_ERR        *p_err,
                        int	        deadline,
                        int id)
{
  
    p_tcb->deadline = deadline;
    p_tcb->id = id;
    CPU_STK_SIZE   i;
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();
    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */

    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */


    // Insert the TCB into the AVL tree
    AVL_insertDeadline(p_tcb);
    //OSSched();
}

// Remove the task from the os_ready_list, and invoke OSSched() to schedule the next task
void  OSRrecTaskDel (   OS_TCB  *p_tcb,
                        OS_ERR  *p_err)
{
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* See if trying to delete from ISR                       */
       *p_err = OS_ERR_TASK_DEL_ISR;
        return;
    }
#endif

    if (p_tcb == &OSIdleTaskTCB) {                          /* Not allowed to delete the idle task                    */
        *p_err = OS_ERR_TASK_DEL_IDLE;
        return;
    }

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (p_tcb == &OSIntQTaskTCB) {                          /* Cannot delete the ISR handler task                     */
        *p_err = OS_ERR_TASK_DEL_INVALID;
        return;
    }
#endif

    if (p_tcb == (OS_TCB *)0) {                             /* Delete 'Self'?                                         */
        CPU_CRITICAL_ENTER();
        p_tcb  = OSTCBCurPtr;                               /* Yes.                                                   */
        CPU_CRITICAL_EXIT();
    }

    OS_CRITICAL_ENTER();
    switch (p_tcb->TaskState) {
        case OS_TASK_STATE_RDY: 
             OSRrecRdyListRemove(p_tcb);
             break;

        case OS_TASK_STATE_SUSPENDED:
             break;

        case OS_TASK_STATE_DLY:                             /* Task is only delayed, not on any wait list             */
        case OS_TASK_STATE_DLY_SUSPENDED:
             OS_TickListRemove(p_tcb);
             break;

        case OS_TASK_STATE_PEND:
        case OS_TASK_STATE_PEND_SUSPENDED:
        case OS_TASK_STATE_PEND_TIMEOUT:
        case OS_TASK_STATE_PEND_TIMEOUT_SUSPENDED:
             OS_TickListRemove(p_tcb);
             switch (p_tcb->PendOn) {                       /* See what we are pending on                             */
                 case OS_TASK_PEND_ON_NOTHING:
                 case OS_TASK_PEND_ON_TASK_Q:               /* There is no wait list for these two                    */
                 case OS_TASK_PEND_ON_TASK_SEM:
                      break;

                 case OS_TASK_PEND_ON_FLAG:                 /* Remove from wait list                                  */
                 case OS_TASK_PEND_ON_MULTI:
                 case OS_TASK_PEND_ON_MUTEX:
                 case OS_TASK_PEND_ON_Q:
                 case OS_TASK_PEND_ON_SEM:
                      OS_PendListRemove(p_tcb);
                      break;

                 default:
                      break;
             }
             break;

        default:
            OS_CRITICAL_EXIT();
            *p_err = OS_ERR_STATE_INVALID;
            return;
    }

#if OS_CFG_TASK_Q_EN > 0u
    (void)OS_MsgQFreeAll(&p_tcb->MsgQ);                     /* Free task's message queue messages                     */
#endif

    OSTaskDelHook(p_tcb);                                   /* Call user defined hook                                 */

#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListRemove(p_tcb);
#endif
    OSTaskQty--;                                            /* One less task being managed                            */

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */
    p_tcb->TaskState = (OS_STATE)OS_TASK_STATE_DEL;         /* Indicate that the task was deleted                     */

    OS_CRITICAL_EXIT_NO_SCHED();
    OSSched();                                              /* Find new highest priority task                         */

    *p_err = OS_ERR_NONE;
}

// Remove the TCB node from the os_ready_list
void  OSRrecRdyListRemove(OS_TCB *p_tcb)
{   
    OS_RDY_LIST  *p_rdy_list;
    OS_TCB       *p_tcb1;
    OS_TCB       *p_tcb2;

    p_rdy_list = &OSRdyList[p_tcb->Prio];

    p_tcb1     = p_tcb->PrevPtr;                            /* Point to next and previous OS_TCB in the list          */
    p_tcb2     = p_tcb->NextPtr;
    if (p_tcb1 == (OS_TCB *)0) {                            /* Was the OS_TCB to remove was at the head?              */
        if (p_tcb2 == (OS_TCB *)0) {                        /* Yes, was it the only OS_TCB?                           */
            p_rdy_list->NbrEntries = (OS_OBJ_QTY)0;         /*      Yes, no more entries                              */
            p_rdy_list->HeadPtr    = (OS_TCB   *)0;
            p_rdy_list->TailPtr    = (OS_TCB   *)0;
            OS_PrioRemove(p_tcb->Prio);
        } else {
            p_rdy_list->NbrEntries--;                       /*      No,  one less entry                               */
            p_tcb2->PrevPtr        = (OS_TCB   *)0;         /*           adjust back link of new list head            */
            p_rdy_list->HeadPtr    = p_tcb2;                /*           adjust OS_RDY_LIST's new head                */
        }
    } else {
        p_rdy_list->NbrEntries--;                           /* No,  one less entry                                    */
        p_tcb1->NextPtr = p_tcb2;
        if (p_tcb2 == (OS_TCB *)0) {
            p_rdy_list->TailPtr = p_tcb1;                   /*      Removing the TCB at the tail, adj the tail ptr    */
        } else {
            p_tcb2->PrevPtr     = p_tcb1;
        }
    }
    p_tcb->PrevPtr = (OS_TCB *)0;
    p_tcb->NextPtr = (OS_TCB *)0;
}