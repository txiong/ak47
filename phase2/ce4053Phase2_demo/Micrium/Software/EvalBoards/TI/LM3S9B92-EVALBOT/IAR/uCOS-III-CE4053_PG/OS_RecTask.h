#ifndef OS_RECTASK_H
#define OS_RECTASK_H

#include <stdio.h>
#include <os.h>
#include "os_scheduler.h"

#define LCHILD(x) 2 * x + 1
#define LCHILD(x) 2 * x + 1
#define RCHILD(x) 2 * x + 2
#define PARENT(x) (x - 1) / 2


typedef struct node {
    OS_TCB              *p_tcb;
    CPU_CHAR            *p_name;
    OS_TASK_PTR         p_task;
    void                *p_arg;
    OS_PRIO             prio;
    CPU_STK             *p_stk_base;
    CPU_STK_SIZE        stk_limit;
    CPU_STK_SIZE        stk_size;
    OS_MSG_QTY          q_size;
    OS_TICK             time_quanta;
    void                *p_ext;
    OS_OPT              opt;
    CPU_INT08U          time;
    int                 id;
    int                 interval;
    int                 deadline;
} NODE;

void initSystemCeiling();
int getSystemCeiling();
void updateSystemCeiling(int SC);
void insertBinaryHeap(  OS_TCB        *p_tcb,
                        CPU_CHAR      *p_name,
                        OS_TASK_PTR    p_task,
                        void          *p_arg,
                        OS_PRIO        prio,
                        CPU_STK       *p_stk_base,
                        CPU_STK_SIZE   stk_limit,
                        CPU_STK_SIZE   stk_size,
                        OS_MSG_QTY     q_size,
                        OS_TICK        time_quanta,
                        void          *p_ext,
                        OS_OPT         opt,
                        int id,
                        int time, 
                        int interval,
                        int deadline);
void swap(NODE *node1, NODE *node2);
//void insertBinaryHeap(int id,int time, int interval,int deadline,OS_PRIO prio);
void update();
void heapify(int root);
NODE *getMin();
void recursiveTaskDispatcher();

void  OSRecTaskCreate (	OS_TCB        *p_tcb,
                        CPU_CHAR      *p_name,
                        OS_TASK_PTR    p_task,
                        void          *p_arg,
                        OS_PRIO        prio,
                        CPU_STK       *p_stk_base,
                        CPU_STK_SIZE   stk_limit,
                        CPU_STK_SIZE   stk_size,
                        OS_MSG_QTY     q_size,
                        OS_TICK        time_quanta,
                        void          *p_ext,
                        OS_OPT         opt,
                        OS_ERR        *p_err,
                        int	        deadline,
                        int id);

void  OSRrecTaskDel (OS_TCB  *p_tcb,
                    OS_ERR  *p_err);

void  OSRrecRdyListRemove(OS_TCB *p_tcb);
#endif